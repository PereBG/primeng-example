import {NgModule}      from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule}    from '@angular/http';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LocationStrategy,HashLocationStrategy} from '@angular/common';
import {AppRoutes} from './app.routes';
import 'rxjs/add/operator/toPromise';

import {AppComponent}  from './app.component';
import {AppMenuComponent,AppSubMenu}  from './app.menu.component';
import {AppSideBarComponent} from './app.sidebar.component';
import {AppSidebarTabContent}  from './app.sidebartabcontent.component';
import {AppTopBar}  from './app.topbar.component';
import {AppFooter}  from './app.footer.component';
import {EmptyDemo} from './demo/view/emptydemo';
import {UtilsDemo} from './demo/view/utilsdemo';
import {Documentation} from './demo/view/documentation';

import {InputTextModule} from 'primeng/primeng';
import { CarmanagerComponent } from './demo/view/carmanager/carmanager.component';
import {DataTableModule, ButtonModule, DialogModule} from 'primeng/primeng';
import {CarserviceService} from './carservice.service';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutes,
        HttpModule,
        BrowserAnimationsModule,
        InputTextModule,
        DataTableModule,
        ButtonModule,
        DialogModule
        
    ],
    declarations: [
        AppComponent,
        AppMenuComponent,
        AppSubMenu,
        AppSideBarComponent,
        AppSidebarTabContent,
        AppTopBar,
        AppFooter,
        EmptyDemo,
        UtilsDemo,
        Documentation,
        CarmanagerComponent
    ],
    providers: [
        {provide: LocationStrategy, useClass: HashLocationStrategy},
        CarserviceService
    ],
    bootstrap:[AppComponent]
})
export class AppModule { }
