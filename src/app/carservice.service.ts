import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Car} from './demo/domain/car';

@Injectable()
export class CarserviceService {

 

  constructor(private http:Http) { }

  loadCars(){
    return this.http.get('assets/cardata/cars.json')
    .toPromise().then(res => <Car[]> res.json().data);
  }

}
