import { Component, OnInit } from '@angular/core';
import {CarserviceService} from '../../../carservice.service';
import {Car} from '../../domain/car';

@Component({
  selector: 'app-carmanager',
  templateUrl: './carmanager.component.html',
  styleUrls: ['./carmanager.component.scss']
})
export class CarmanagerComponent implements OnInit {

  cars: Car[];

  selectedCar: Car;

  visibleDialog: boolean = false;

  addDialog: boolean = false;

  constructor(private service: CarserviceService) { }

  ngOnInit() {
    this.service.loadCars().then(data => this.cars = data);
    this.selectedCar = new PrimeCar();
  }

  addCar(){
    console.log(this.selectedCar)
    this.cars = [...this.cars, this.selectedCar];
    this.selectedCar = new PrimeCar();
    this.visibleDialog = false;
    this.addDialog = false;
  }

  onAddClick(event: Event){
    this.selectedCar = new PrimeCar();
    this.visibleDialog = true;
    this.addDialog = true;
  }

  onRowSelect(event: Event){
    this.visibleDialog = true;
        this.addDialog = false;
  }

}

class PrimeCar implements Car{
  public vin: string;
  public year: number;
  public brand: string;
  public color: string;
  public price: number;

}

