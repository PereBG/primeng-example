import {Routes,RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {EmptyDemo} from './demo/view/emptydemo';
import {UtilsDemo} from './demo/view/utilsdemo';
import {Documentation} from './demo/view/documentation';
import {CarmanagerComponent} from './demo/view/carmanager/carmanager.component';

export const routes: Routes = [
    {path: '', component: EmptyDemo},
    {path: 'empty', component: EmptyDemo},
    {path: 'utils', component: UtilsDemo},
    {path: 'documentation', component: Documentation},
    {path: 'car', component: CarmanagerComponent}
];

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
